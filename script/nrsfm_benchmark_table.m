function [ table ] = nrsfm_benchmark_table(data)
    measure = data(:, 1);
    algorithm = data(:, 3);
    projection = data(:, 4);
    scene = data(:, 5);
    camera = data(:, 6);
    %[measure_com, algorithm_com, scene_com, projection_com, camera_com] = aggregate_nrsfm(data_in, key1);
    %[measure_acc, algorithm_acc, scene_acc, projection_acc, camera_acc] = aggregate_nrsfm(data_in, key2);

    unique_algo = unique(algorithm);
    unique_scene = unique(scene);
    unique_proj = unique(projection);
    table = struct();
    for i = 1:numel(unique_proj)
        p = strcmp(projection, unique_proj{i});
        %pa = strcmp(projection_acc, unique_proj{i});
        % Add two columns for name and mean
        data = cell(numel(unique_algo), numel(unique_scene) + 2);

        for j = 1:numel(unique_algo)
            al = strcmp(algorithm, unique_algo{j});
            %aa = strcmp(algorithm_acc, unique_algo{j});
            for k = 1:numel(unique_scene)
               sc = strcmp(scene, unique_scene{k});
               %sa = strcmp(scene_acc, unique_scene{k});
               m = measure(p & al & sc); %+ measure_acc(pa & aa & sa);
               m = cell2mat(m);
               data{j, k + 2} = rms(m(~isnan(m)));
            end
            data{j, 2} = mean([data{j, 3:end}]);
            data{j, 1} = unique_algo{j};
        end
        data = remove_nan_entry(data);
        data(:, 1) = web_format_name(data(:, 1));
        table.(unique_proj{i}) = cell2table(data, 'VariableNames', [{'Algorithm'}, {'Mean_RMS'}, unique_scene']);%data;
    end
    
end

function[tab_clean] = remove_nan_entry(tab_dirty)
    dirty_rows = sum(isnan(cell2mat(tab_dirty(:, 2:end))), 2) ~= 0;
    tab_clean = tab_dirty;
    tab_clean(dirty_rows, :) = [];
end