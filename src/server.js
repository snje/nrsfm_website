var rx = require('rx');
var express = require('express');
var rxnode = require('rx-node');
var fs = require('fs');
var bibtex_parser = require("bibtex-parser")

// Read the bibtext file into memory
const bibtext_obj = bibtex_parser(fs.readFileSync('data/biblio.bib').toString());

var app = express();

app.set('view engine', 'pug');

app.use("/style", express.static(__dirname + "/../style"))
app.use("/module", express.static(__dirname + '/../module'))
app.use("/src", express.static(__dirname + '/../src'))
app.use("/image", express.static(__dirname + '/../image'))
app.use("/data", express.static(__dirname + '/../data'))

app.get('/citation', (req, res) => {
    //const test = {yo: 'verden', super: 'duper'};
    const str = JSON.stringify(bibtext_obj);
    res.send(str);
})
const subpages = [
    'home', 'submit', 'dataset', 'test', 'benchmark', 'dates', 'program',
    'people'
];
app.get('/', (req, res) => {
    res.render('home');
});
subpages.map(key => {
    app.get('/' + key, (req, res) => {
        res.render(key);
    })
})


var server = app.listen(8080, "127.0.0.1", () => {
    var a = server.address();

    console.log("Server listening at http://%s:%s", a.address, a.port)
});
