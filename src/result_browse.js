const table = d3.select('#tab_benchmark')

const sorting_key_subject = new Rx.Subject;
const cite_subject = new Rx.Subject();

function update_ui_table(data, key, numformat) {
    table.selectAll('*').remove();
    const thead = table.append('thead');
    const tbody = table.append('tbody');

    //tbody.on("mouseout", (e) => console.log("nope"));
    const key_weight = {date: 2, algorithm: 1};
    function fetch_weight(k) {
        return key_weight[k] == undefined ? 0 : key_weight[k];
    }
    //const fetch_weight = (k) => key_weight[k] == undefined ? 0 : key_weight[k];
    const sorted_keys = Object.keys(data[0]).sort(function(k1, k2){
        return fetch_weight(k1) < fetch_weight(k2);
    });

    thead.append('tr')
        .selectAll('th')
        .data(sorted_keys)
        .enter().append('th')
        .style("width", "300px")
        .append('button')
        .attr("class", function(d) {return d == key ? "select" : "item"})
        .style("width", "100%")
        .style("padding-right", "5px")
        .style("padding-left", "5px")
        //.style("border", "1px solid #0d0b42")
        .on("click", function(d) {return sorting_key_subject.onNext(d)})
        .text(function(d) {return d.replace('_', ' ')});

    const rows = tbody.selectAll("tr")
        .data(data)
        .enter().append("tr")
        .style("background-color", function(d, i) {return i % 2 ? "#ffffff": "#eff0f1"})
        .on("mouseover", function(d) {return cite_subject.onNext(d)});

    const cells = rows.selectAll('td')
        .data(function(row) {
            const keys = sorted_keys;
            const d = keys.map(function(k) {return row[k]});
            for (var i = 1; i < d.length; i++) {
                d[i] = numformat(parseFloat(d[i]).toFixed(2));
            }
            return d;
        })
        .enter().append('td')
        .attr("class", "num_cell")
        .text(function(t) {return t});
}

function find_button_element(key) {
    return document.querySelector("#b_" + key);
}
function key2clickevent(key) {
    const obj = find_button_element(key);
    return Rx.Observable.fromEvent(obj, "click").map(function() {return key});
}
function create_option_stream(keys) {
    return Rx.Observable.merge(keys.map(key2clickevent))
        .startWith(keys[0])
        .distinctUntilChanged();
}

function toggle_ui_selection(stream, ids) {
    return ids.map(function(key) {
        const domobj = find_button_element(key);
        return stream.map(function(id) {return id == key ? 'select': 'item'})
            .subscribe(function(class_name) {return domobj.className = class_name});
    })
}

function init_option_pane(ids) {
    const stream = create_option_stream(ids);
    toggle_ui_selection(stream, ids);
    return stream
}

const metric_ids = ['distance', 'time']
const projection_ids = ['orthogonal', 'perspective']
const occlusion_ids = ['none', 'self']

const metric_stream = init_option_pane(metric_ids);
const projection_stream = init_option_pane(projection_ids);
const occlusion_stream = init_option_pane(occlusion_ids)
/*
const set_stream = create_option_stream(set_ids);
const metric_stream = create_option_stream(metric_ids);
const interaction_stream = create_option_stream(interaction_ids);

toggle_ui_selection(set_stream, set_ids);
toggle_ui_selection(metric_stream, metric_ids);
toggle_ui_selection(interaction_stream, interaction_ids);

const key2csv = {
    [create_csv_key('sparse', 'frobenius', 'scene')]: 'sparse_frobenius_scene.csv',
    [create_csv_key('sparse', 'time', 'scene')]: 'sparse_time_scene.csv'
}
*/

function make_key_from_id(metric, proj, occ) {
    const id2key = {
        distance: 'dist',
        time: 'time',
        orthogonal: 'ort',
        perspective: 'per',
        none: 'full',
        self: 'miss'
    };
    return id2key[metric] + '_' + id2key[occ] + '_' + id2key[proj] + '.csv';
}

const csv_url_stream = Rx.Observable.combineLatest(
        [metric_stream, projection_stream, occlusion_stream],
        make_key_from_id
).map(function(key){return '/data/' + key});


function fetch_json(url) {
    return Rx.Observable.create(function (observer) {
        const req = new XMLHttpRequest();
        req.onreadystatechange = function() {
            if (req.readyState == 4 && req.status == 200) {
                try {
                    const json_obj = JSON.parse(req.responseText);
                    observer.onNext(json_obj);
                } catch (e) {
                    console.log("Recieved citation object was not of JSON format");
                    observer.onError();
                }
                observer.onCompleted();
            } else {
                //observer.onError();
            }
        }
        req.open("GET", url, true);
        req.send(null);
    });
};

function fetch_csv(url) {
    return Rx.Observable.create(function(observer) {
        d3.csv(url, function(err, data) {
            observer.onNext(data);
            observer.onCompleted();
        });
    });
}

const default_bibentry =
{
    bib: {
        AUTHOR: 'N/A',
        TITLE: 'N/A',
    },
    acro: 'N/A'
};
const citation_stream = Rx.Observable.just('/citation').flatMap(fetch_json)
//Rx.Observable.interval(1000)
    //.take(1)
    //.map(function() { return '/data/car.csv' })
Rx.Observable.combineLatest(
    [citation_stream, cite_subject.distinctUntilChanged()],
    function(bibtex_json, cite_entry) {
        const acro = cite_entry.Algorithm;
        if (acro == undefined) return default_bibentry;
        const key = acro.toUpperCase().replace(" ", "");
        var bibentry = bibtex_json[key];
        bibentry = bibentry == undefined ? default_bibentry : bibentry
        return {bib: bibentry, acro: acro};
    }
).subscribe(function(v) {
    const b = v.bib;
    const acro = v.acro;

    var value = [b.AUTHOR, b.TITLE, b.JOURNAL].reduce(function(k1, k2) {
        k1 = k1 == undefined ? "" : k1;
        k2 = k2 == undefined ? "" : k2;
        return k1 + ". " + k2;
    });
    value = value + ", " + b.YEAR + ".";

    d3.select('#h_acro')
        .data([acro])
        .text(function(t) {return t});
    d3.select('#p_descript')
        .data([value])
        .text(function(t) {return t});
});

const table_stream = csv_url_stream.flatMap(fetch_csv);

function add_date(citations, r) {
    const algo = r.Algorithm.toUpperCase().replace(" ", "");
    const c = citations[algo];
    const t = c == undefined ? "1970" : c.YEAR;
    r.date = t == undefined ? "1970" : t;
    return r;
}

function time_format(d) {
    return d + 's';
}
function default_format(d) {
    return d + 'mm';
}

Rx.Observable.combineLatest(
    [
        table_stream, citation_stream,
        sorting_key_subject.startWith('nope')
            .scan(function(agg, key) {
                return {
                    key: key, reverse: agg.key == key ? !agg.reverse : false
                }
            }, {key: 'nope', reverse: false}),
        metric_stream.map(function(id) {return id == 'time' ? time_format : default_format})
    ],
    function(rows, citations, key_rev_pair, format) {
        //rows = rows.map(r => add_date(citations, r));
        const key = key_rev_pair.key;
        function fetch_val(r) {
            const v = r[key];
            const n = parseFloat(v);
            return [v, n];
        }
        function sorter(r1, r2) {
            v1 = fetch_val(r1);
            v2 = fetch_val(r2);
            if (v2[0] == undefined) return false;
            return isNaN(v2[1]) || isNaN(v1[1]) ? v2[0].localeCompare(v1[0]): v1[1] - v2[1];
        };
        const sort_row = rows.sort(sorter);
        return {
            data: key_rev_pair.reverse ? sort_row.reverse(): sort_row,
            key: key,
            format: format
        }
    }
).subscribe(function(d) {update_ui_table(d.data, d.key, d.format)});
