const rx = Rx;

const homepane = document.querySelector('#panehome');

const button2page = {
    panechallenge: '/challenge',
    panesubmit: '/submit',
    panedates: '/dates',
    paneprogram: '/program',
    panedata: '/dataset',
    paneresult: '/result',
    panepeople: '/people',
};

function resize_content_frame() {
    const ifobj = document.getElementById('contentframe');
    const h = (ifobj.contentWindow.document.body.offsetHeight + 50) + 'px';
    console.log(h)
    ifobj.style.height = h;
};


document.getElementById('contentframe').onload = resize_content_frame;


const passive_color = {
    'background-color': "#5050ff",
    color: "white",
    //border: "5px solid #5050c8",
};
const select_color = {
    'background-color': "#dfe137",
    //border: "5px solid #e1d137",
    color: "#5050c8"
}

function set_button_properties(domobj, prop) {
    for (var key in prop) {
        domobj.style[key] = prop[key];
    }
}

let button2page_sub = {};
for (var key in button2page) {
    const domid = '#' + key;
    const domobj = document.querySelector(domid);
    const url = button2page[key];
    button2page_sub[key] = rx.DOM.click(domobj)
        .subscribe(() => {
            const ifobj = document.getElementById('contentframe');
            ifobj.src = url;
            for (var _key in button2page) {
                //if (_key != key) {
                    set_button_properties(
                        document.querySelector('#' + _key), passive_color
                    );
                //}
            }
            set_button_properties(domobj, select_color);
            //domobj.style['background-color'] = select_color.bg;
            //domobj.style['color'] = select_color.text;
            //domobj.style['border'] = select_color.text;
        });
}
